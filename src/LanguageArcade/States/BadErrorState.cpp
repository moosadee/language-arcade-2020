#include "BadErrorState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Platform.hpp"

BadErrorState::BadErrorState()
{
}

void BadErrorState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "BadErrorState::Init", "function-trace" );
    CursorState::Init( name, false );
}

void BadErrorState::Setup()
{
    chalo::Logger::Out( "", "BadErrorState::Setup", "function-trace" );
    IState::Setup();
    CursorState::Setup();

    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/UI/menu-icons.png" );

    chalo::MenuManager::LoadTextMenu( "error.chalomenu" );

    std::string font = chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" );
    sf::Color col = sf::Color::White;

    chalo::MenuManager::AddLabel( "asdf", "Error", col, font, 20, sf::Vector2f( 10, 100 ), chalo::Messager::Get( "error" ), false, true );

    chalo::InputManager::Setup();
}

void BadErrorState::Cleanup()
{
    chalo::Logger::Out( "", "BadErrorState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void BadErrorState::Update()
{
    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Demo select menu
    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupstate" );
    }
}

void BadErrorState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
    CursorState::Draw( window );
}



