#include "CharacterCreatorState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Platform.hpp"

CharacterCreatorState::CharacterCreatorState()
{
}

void CharacterCreatorState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "CharacterCreatorState::Init", "function-trace" );
    CursorState::Init( name, false );
}

void CharacterCreatorState::Setup()
{
    chalo::Logger::Out( "", "CharacterCreatorState::Setup", "function-trace" );
    IState::Setup();
    CursorState::Setup();

    chalo::TextureManager::Add( "bg",                      "Content/Graphics/UI/menubg.png" );
    chalo::TextureManager::Add( "logo",                    "Content/Graphics/UI/logo-moosadee-small.png" );
    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square2",          "Content/Graphics/UI/button-square2.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "button-frame",            "Content/Graphics/UI/button-frame.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/UI/menu-icons.png" );
    chalo::TextureManager::Add( "menu-icons2",             "Content/Graphics/UI/menu-icons2.png" );

    chalo::MenuManager::LoadTextMenu( "charactercreator.chalomenu" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    chalo::InputManager::Setup();
}

void CharacterCreatorState::Cleanup()
{
    chalo::Logger::Out( "", "CharacterCreatorState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void CharacterCreatorState::Update()
{
    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Demo select menu
    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupstate" );
    }
}

void CharacterCreatorState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
    CursorState::Draw( window );
}



