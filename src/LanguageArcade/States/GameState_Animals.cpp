#include "GameState_Animals.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Platform.hpp"
#include "../chalo-engine/Utilities/HelperFunctions.hpp"

GameState_Animals::GameState_Animals()
{
}

void GameState_Animals::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "GameState_Animals::Init", "function-trace" );
    CursorState::Init( name, false );
}

void GameState_Animals::Setup()
{
    chalo::Logger::Out( "", "GameState_Animals::Setup", "function-trace" );
    IState::Setup();
    CursorState::Setup();

    chalo::TextureManager::Add( "hudGradient", "Content/Graphics/UI/hud-gradient.png" );
    chalo::TextureManager::Add( "button-long", "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/UI/menu-icons.png" );

    m_avatar.setTexture( chalo::TextureManager::AddAndGet( "avatar", "Content/Graphics/Characters/simple-bunny.png" ) );
    m_helper.setTexture( chalo::TextureManager::AddAndGet( "helper", "Content/Graphics/Characters/simple-rachel.png" ) );
    m_background.setTexture( chalo::TextureManager::AddAndGet( "bg", "Content/Graphics/Backgrounds/grassy.png" ) );
    m_wordBalloon.setTexture( chalo::TextureManager::AddAndGet( "wordBalloon", "Content/Graphics/UI/word-balloon.png" ) );

    chalo::MenuManager::LoadTextMenu( "game-hud.chalomenu" );

    m_playerOriginalPosition = sf::Vector2f( 1000, 350 );
    m_avatar.setScale( 2.0, 2.0 );
    m_avatar.setPosition( m_playerOriginalPosition );
    m_helper.setScale( 2.0, 2.0 );
    m_helper.setPosition( 1100, 100 );

    m_wordBalloon.setPosition( 550, 10 );

    chalo::Logger::Out( "LANGUAGE_MAIN: " + chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "ChaloEngineProgram::Setup" );
    chalo::Logger::Out( "LANGUAGE_TARGET: " + chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), "ChaloEngineProgram::Setup" );
    chalo::Logger::Out( "language_name: " + chalo::LanguageManager::Text( "language_name" ) );
    chalo::Logger::Out( "language_name (target): " + chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), "language_name" ) );

    m_helperLanguage.Setup(
        "lblHelper",
        chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" ),
        15,
        sf::Color::White,
        sf::Vector2f( 10, 700 ),
        chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "language_name" )
    );
    m_targetLanguage.Setup(
        "lblTarget",
        chalo::ConfigManager::Get( "LANGUAGE_TARGET_FONT" ),
        15,
        sf::Color::White,
        sf::Vector2f( 200, 700 ),
        chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), "language_name" )
    );

    m_transitionTimer = 0;
    m_setupTimer = 0;
    m_attackTimer = 0;
    m_gameStats.Setup();

    chalo::InputManager::Setup();
    SetupVocabulary();

    UpdateLabels();
    GetNextQuestion();
}

void GameState_Animals::UpdateLabels()
{
    std::string updatedText = chalo::LanguageManager::Text(
        chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "total_questions_answered" ) + " "
        + chalo::StringUtility::IntegerToString( m_gameStats.GetTotalQuestionsAsked() );
    chalo::MenuManager::GetLabel( "lblTotalQuestions" ).SetText( updatedText );

    updatedText = chalo::LanguageManager::Text(
        chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "total_correct_first_time" ) + " "
        + chalo::StringUtility::IntegerToString( m_gameStats.GetTotalCorrectFirstAsk() );
    chalo::MenuManager::GetLabel( "lblTotalCorrectFirstTime" ).SetText( updatedText );

    updatedText = chalo::LanguageManager::Text(
        chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "total_remaining_vocabulary" ) + " "
        + chalo::StringUtility::IntegerToString( m_questionList.size() );
    chalo::MenuManager::GetLabel( "lblTotalRemaining" ).SetText( updatedText );
}

void GameState_Animals::Cleanup()
{
    chalo::Logger::Out( "", "GameState_Animals::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void GameState_Animals::Update()
{
    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Demo select menu
    if ( clickedButton == "btnBack" )
    {
        //SetGotoState( "startupstate" );
        EndMinigame();
    }

    for ( unsigned int i = 0; i < m_questionButtonNames.size(); i++ )
    {
        if ( CurrentlyAnimating() )
        {
            break;
        }

        if ( clickedButton == m_questionButtonNames[i] )
        {
            m_attackingIndex = i;

            // Is this the correct item?
            if ( i == m_correctIndex )
            {
                // Correct
                chalo::Logger::Out( "Correct selection! " + m_questionButtonNames[i], "GameState_Animals::Update" );
                //Transition_QuestionEnd_Begin();
                m_answeredCorrectly = true;
                m_gameStats.AddToQuestionsAsked();
                Transition_Attack_Begin();

                if ( m_rightFirstTime )
                {
                    m_gameStats.AddToTotalCorrectFirstAsk();
                }
            }
            else
            {
                // Incorrect
                chalo::Logger::Out( "Incorrect selection! " + m_questionButtonNames[i], "GameState_Animals::Update" );
                m_answeredCorrectly = false;
                Transition_Attack_Begin();
                m_rightFirstTime = false;

                if ( m_firstTimeAnswering )
                {
                    m_gameStats.AddToTopicTimesWrong( m_correctName );

                    // m_correctName
                    m_gameStats.AddToWrongVocabulary(
                        chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), "animal_" + m_correctName )
                        + " (" + m_correctName + ")" );

                    m_firstTimeAnswering = false;
                }
            }
        }
    }

    if ( CurrentlyAnimating() )
    {
        m_question.SetIsVisible( false );
    }
    else
    {
        m_question.SetIsVisible( true );
    }

    Transition_NewQuestion_Update();
    Transition_Attack_Update();
    Transition_QuestionEnd_Update();
}

bool GameState_Animals::CurrentlyAnimating()
{
    return ( m_transitionTimer > 0 || m_attackTimer > 0 || m_setupTimer > 0 );
}

void GameState_Animals::Transition_NewQuestion_Begin()
{
    m_setupTimer = 50;
}

void GameState_Animals::Transition_NewQuestion_Update()
{
    if ( m_setupTimer > 0 )
    {
        m_setupTimer--;
        for ( auto& animal : m_questionButtonNames )
        {
            chalo::UIButton& animalButton = chalo::MenuManager::GetButton( "animals", animal );
            sf::IntRect pos = animalButton.GetPositionRect();
            pos.left += 7;
            animalButton.SetPosition( pos );
        }
    }
}

void GameState_Animals::Transition_Attack_Begin()
{
    m_attackTimer = 50;
}

void GameState_Animals::Transition_Attack_Update()
{
    if ( m_attackTimer > 0 )
    {
        m_attackTimer--;

        int moveAmountY = 20;
        int moveAmountX = 30;
        sf::Vector2f pos = m_avatar.getPosition();
        if ( m_attackTimer >= 25 )
        {
            // Move player toward attack target
            sf::Vector2f target = m_questionButtonPositions[m_attackingIndex];

            if ( target.x < pos.x )
            {
                pos.x -= moveAmountX;
            }
            else if ( target.x > pos.x )
            {
                pos.x += moveAmountX;
            }

            if ( target.y + moveAmountY < pos.y )
            {
                pos.y -= moveAmountY;
            }
            else if ( target.y - moveAmountY > pos.y )
            {
                pos.y += moveAmountY;
            }
        }
        else
        {
            // Move player back to original position

            if ( m_playerOriginalPosition.x < pos.x )
            {
                pos.x -= moveAmountX;
            }
            else if ( m_playerOriginalPosition.x > pos.x )
            {
                pos.x += moveAmountX;
            }

            if ( m_playerOriginalPosition.y < pos.y )
            {
                pos.y -= moveAmountY;
            }
            else if ( m_playerOriginalPosition.y > pos.y )
            {
                pos.y += moveAmountY;
            }

        }
        m_avatar.setPosition( pos );

        if ( m_attackTimer == 0 )
        {
            m_avatar.setPosition( m_playerOriginalPosition );

            if ( m_answeredCorrectly )
            {
                Transition_QuestionEnd_Begin();
            }
        }
    }
}

void GameState_Animals::Transition_QuestionEnd_Begin()
{
    m_transitionTimer = 50;
    m_question.SetIsVisible( false );
    m_gameStats.AddToTopicTimesRight( m_correctName );

    if ( !m_rightFirstTime )
    {
        // Put it back in the list
        m_questionList.push_back( m_correctName );
    }

    UpdateLabels();
}

void GameState_Animals::Transition_QuestionEnd_Update()
{
    if ( m_transitionTimer > 0 )
    {
        m_transitionTimer--;
        for ( auto& animal : m_questionButtonNames )
        {
            chalo::UIButton& animalButton = chalo::MenuManager::GetButton( "animals", animal );
            sf::IntRect pos = animalButton.GetPositionRect();
            pos.left -= 7;
            animalButton.SetPosition( pos );
        }

        if ( m_transitionTimer == 0 )
        {
            GetNextQuestion();
        }
    }
}

void GameState_Animals::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
    chalo::DrawManager::AddSprite( m_background );
    chalo::DrawManager::AddSprite( m_helper );
    chalo::DrawManager::AddSprite( m_avatar );
    if ( !CurrentlyAnimating() )
    {
        chalo::DrawManager::AddSprite( m_wordBalloon );
    }
    chalo::DrawManager::AddUILabel( m_question );
//    chalo::DrawManager::AddUILabel( m_targetLanguage );
//    chalo::DrawManager::AddUILabel( m_helperLanguage );
    CursorState::Draw( window );
}

void GameState_Animals::SetupVocabulary()
{
    m_questionList = std::vector<std::string>( {
        "ant",
        "bear",
        "bee",
        "bird",
        "butterfly",
        "cat",
        "chicken",
        "cow",
        "crab",
        "crocodile",
        "dog",
        "dolphin",
        "elephant",
        "fox",
        "frog",
        "kangaroo",
        "lion",
        "llama",
        "monkey",
//        "moose",
//        "mouse",
        "owl",
        "penguin",
        "pig",
        "rabbit",
        "seal",
//        "sheep",
        "skunk",
        "snake",
        "squirrel",
        "turkey",
        "turtle",
        "whale"
    } );

    for ( auto& animal : m_questionList )
    {
        m_questionListFull.push_back( animal );
        chalo::TextureManager::Add( animal, "Content/Graphics/Animals/" + animal + ".png" );
    }
}

void GameState_Animals::EndMinigame()
{
    chalo::Logger::Out( "function start", "GameState_Animals::EndMinigame" );

    // Track stats
    chalo::Messager::Set( "total_questions", chalo::StringUtility::IntegerToString( m_gameStats.GetTotalQuestionsAsked() ) );
    chalo::Messager::Set( "total_correct", chalo::StringUtility::IntegerToString( m_gameStats.GetTotalCorrectFirstAsk() ) );
    chalo::Messager::Set( "vocabulary_missed", m_gameStats.GetWrongVocabularyString() );

    SetGotoState( "minigameend" );
}

void GameState_Animals::GetNextQuestion()
{
    chalo::Logger::Out( "function start", "GameState_Animals::GetNextQuestion" );
    chalo::Logger::Out( "Questions left: " + chalo::StringUtility::IntegerToString( m_questionList.size() ), "GameState_Animals::GetNextQuestion" );
    for ( auto& animal : m_questionList )
    {
        chalo::Logger::Out( "Question--: " + animal, "GameState_Animals::GetNextQuestion" );
    }

    if ( m_questionList.size() == 0 )
    {
        EndMinigame();
        return;
    }

//    m_animalButtons.clear();
    m_questionButtonNames.clear();
    m_rightFirstTime = true;
    m_firstTimeAnswering = true;

    // Get the correct animal
    int correctAnimal = chalo::GetRandomNumber( 0, m_questionList.size()-1 );
    std::string correctAnimalName = m_questionList[correctAnimal];
    m_correctName = correctAnimalName;
    m_questionList.erase( m_questionList.begin() + correctAnimal );

    // Get two additional animals
    int wrongAnimal1 = chalo::GetRandomNumber( 0, m_questionListFull.size()-1 );
    std::string wrongAnimal1Name = m_questionListFull[wrongAnimal1];
    while ( wrongAnimal1Name == correctAnimalName )
    {
        // Eww I don't like this while loop. TODO: Put safeguard in to prevent infinite looping.
        wrongAnimal1 = chalo::GetRandomNumber( 0, m_questionListFull.size()-1 );
        wrongAnimal1Name = m_questionListFull[wrongAnimal1];
    }

    int wrongAnimal2 = chalo::GetRandomNumber( 0, m_questionListFull.size()-1 );
    std::string wrongAnimal2Name = m_questionListFull[wrongAnimal2];
    while ( wrongAnimal2Name == correctAnimalName || wrongAnimal2Name == wrongAnimal1Name )
    {
        // Eww I don't like this while loop. TODO: Put safeguard in to prevent infinite looping.
        wrongAnimal2 = chalo::GetRandomNumber( 0, m_questionListFull.size()-1 );
        wrongAnimal2Name = m_questionListFull[wrongAnimal2];
    }

    chalo::Logger::Out( "correctAnimalName: " + correctAnimalName, "GameState_Animals::GetNextQuestion" );
    chalo::Logger::Out( "wrongAnimal1: " + wrongAnimal1Name, "GameState_Animals::GetNextQuestion" );
    chalo::Logger::Out( "wrongAnimal2: " + wrongAnimal2Name, "GameState_Animals::GetNextQuestion" );

    // Come up with an order the animals show up in
    m_correctIndex = rand() % 3;
    if ( m_correctIndex == 0 )
    {
        m_questionButtonNames.push_back( correctAnimalName );
        m_questionButtonNames.push_back( wrongAnimal1Name );
        m_questionButtonNames.push_back( wrongAnimal2Name );
    }
    else if ( m_correctIndex == 1 )
    {
        m_questionButtonNames.push_back( wrongAnimal1Name );
        m_questionButtonNames.push_back( correctAnimalName );
        m_questionButtonNames.push_back( wrongAnimal2Name );
    }
    else
    {
        m_questionButtonNames.push_back( wrongAnimal1Name );
        m_questionButtonNames.push_back( wrongAnimal2Name );
        m_questionButtonNames.push_back( correctAnimalName );
    }

    // Generate phrase
    std::string question = chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), "animal_phrase_" + correctAnimalName );
    std::string font = chalo::ConfigManager::Get( "LANGUAGE_TARGET_FONT" );

    chalo::Logger::Out( "Setup question \"" + question + "\" with font \"" + font + "\"", "GameState_Animals::GetNextQuestion" );
    // ( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::string text );
    m_question.SetupW(
        "question",
        chalo::ConfigManager::Get( "LANGUAGE_TARGET_FONT" ),
        30,
        sf::Color::Black,
        sf::Vector2f( 575, 50 ),
        chalo::StringUtility::StringToWString( question )
        );
    m_question.SetIsVisible( false );

    // Set up the sprites
    int x = -200;
    int y = 75;
    int inc = 175;
    for ( auto& animal : m_questionButtonNames )
    {
        chalo::UIButton newButton;

        // const std::string& key, sf::Vector2f basePosition, sf::IntRect dimensions
        newButton.Setup(
            animal,
            sf::Vector2f( x, y ),
            sf::IntRect( 0, 0, 120, 120 )
        );

        newButton.SetupBackground( chalo::TextureManager::Get( animal ), sf::Vector2f( 0, 0 ) );
        chalo::MenuManager::AddButton( "animals", animal, newButton );

        m_questionButtonPositions.push_back( sf::Vector2f( x, y - 60 ) );

        y += inc;
    }

    // Add to the stats
    m_gameStats.AddToTopicTimesAsked( correctAnimalName );

    UpdateLabels();

    Transition_NewQuestion_Begin();
}






