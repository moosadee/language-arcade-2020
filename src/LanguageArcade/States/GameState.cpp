#include "GameState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"

GameState::GameState()
{
}

void GameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "GameState::Init", "function-trace" );
    IState::Init( name );
}

void GameState::Setup()
{
    chalo::Logger::Out( "", "GameState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "block", "Content/Graphics/Demos/block.png" );

    chalo::InputManager::Setup();
}

void GameState::Cleanup()
{
    chalo::Logger::Out( "", "GameState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
}

void GameState::Update()
{
}

void GameState::Draw( sf::RenderWindow& window )
{
}



