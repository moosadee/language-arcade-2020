#include "MinigameEndState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Platform.hpp"

MinigameEndState::MinigameEndState()
{
}

void MinigameEndState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "MinigameEndState::Init", "function-trace" );
    CursorState::Init( name, false );
}

void MinigameEndState::Setup()
{
    chalo::Logger::Out( "", "MinigameEndState::Setup", "function-trace" );
    IState::Setup();
    CursorState::Setup();

    chalo::TextureManager::Add( "bg",                      "Content/Graphics/UI/menubg.png" );
    chalo::TextureManager::Add( "logo",                    "Content/Graphics/UI/logo-moosadee-small.png" );
    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-disabled",         "Content/Graphics/UI/button-disabled.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/UI/menu-icons.png" );

    chalo::MenuManager::LoadTextMenu( "minigameend.chalomenu" );

    chalo::MenuManager::GetLabel( "main", "lblTotalQuestions" ).SetText(
        chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "total_questions" ) + " " + chalo::Messager::Get( "total_questions" ) );

    chalo::MenuManager::GetLabel( "main", "lblTotalCorrect" ).SetText(
        chalo::LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), "total_correct" ) + " " + chalo::Messager::Get( "total_correct" ) );

    chalo::Logger::Out( "vocab_missed: " + chalo::Messager::Get( "vocabulary_missed" ), "MinigameEndState::Setup" );
    chalo::MenuManager::GetLabel( "main", "lblMissed_value" ).SetText( chalo::Messager::Get( "vocabulary_missed" ) );

    chalo::InputManager::Setup();
}

void MinigameEndState::Cleanup()
{
    chalo::Logger::Out( "", "MinigameEndState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void MinigameEndState::Update()
{
    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Demo select menu
    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupstate" );
    }
}

void MinigameEndState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
    CursorState::Draw( window );
}



