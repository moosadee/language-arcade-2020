#ifndef _ANIMAL_GAME_STATE_HPP
#define _ANIMAL_GAME_STATE_HPP

#include "../Learning/GameStats.hpp"
#include "CursorState.hpp"

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

#include <SFML/Audio.hpp>

#include <vector>
#include <string>

class GameState_Animals : public CursorState
{
public:
    GameState_Animals();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    sf::Sprite m_avatar;
    sf::Sprite m_helper;
    sf::Sprite m_background;
    sf::Sprite m_wordBalloon;
    sf::Vector2f m_playerOriginalPosition;

    chalo::UILabel m_question;

    chalo::UILabel m_targetLanguage;
    chalo::UILabel m_helperLanguage;

    std::vector<std::string> m_questionList;
    std::vector<std::string> m_questionListFull;
    //std::vector<chalo::UIButton> m_animalButtons;
    std::vector<std::string> m_questionButtonNames;
    std::vector<sf::Vector2f> m_questionButtonPositions;
    std::map<std::string, int> m_questionStats;
    int m_correctIndex;
    int m_attackingIndex;
    std::string m_correctName;

    int m_setupTimer;
    int m_transitionTimer;
    int m_attackTimer;
    bool m_rightFirstTime;
    bool m_answeredCorrectly;
    bool m_firstTimeAnswering;

    GameStats m_gameStats;

    void SetupVocabulary();
    void GetNextQuestion();
    void EndMinigame();
    void UpdateLabels();

    bool CurrentlyAnimating();

    void Transition_NewQuestion_Begin();
    void Transition_NewQuestion_Update();
    void Transition_Attack_Begin();
    void Transition_Attack_Update();
    void Transition_QuestionEnd_Begin();
    void Transition_QuestionEnd_Update();
};

#endif
