#ifndef _GAME_STATS_HPP
#define _GAME_STATS_HPP

#include <map>

#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/StringUtil.hpp"

struct TopicStats
{
    public:
    TopicStats();
    void Setup();

    int totalTimesAsked;
    int totalTimesRight;
    int totalTimesWrong;
};

class GameStats
{
    public:
    GameStats();

    void Setup();

    void AddToQuestionsAsked();
    void AddToTotalCorrectFirstAsk();
    void AddToWrongVocabulary( std::string topic );
    void AddToTopicTimesAsked( std::string topic );
    void AddToTopicTimesRight( std::string topic );
    void AddToTopicTimesWrong( std::string topic );

    int GetTotalQuestionsAsked();
    int GetTotalCorrectFirstAsk();
    const std::vector<std::string>& GetWrongVocabulary();
    std::string GetWrongVocabularyString();

    private:
    int m_totalQuestionsAsked;
    int m_totalCorrectFirstAsk;

    std::vector<std::string> m_wrongVocabulary;
    std::map<std::string, TopicStats> m_topicStats;
};

#endif
