#include "GameStats.hpp"

TopicStats::TopicStats()
{
    Setup();
}

void TopicStats::Setup()
{
    totalTimesAsked = 0;
    totalTimesRight = 0;
    totalTimesWrong = 0;
}

GameStats::GameStats()
{
    Setup();
}

void GameStats::Setup()
{
    m_totalQuestionsAsked = 0;
    m_totalCorrectFirstAsk = 0;
}

void GameStats::AddToQuestionsAsked()
{
    m_totalQuestionsAsked++;
    chalo::Logger::Out( "Total questions asked is now " + chalo::StringUtility::IntegerToString( m_totalQuestionsAsked ), "GameStats::AddToQuestionsAsked" );
}

void GameStats::AddToTotalCorrectFirstAsk()
{
    m_totalCorrectFirstAsk++;
    chalo::Logger::Out( "Total questions correct first ask " + chalo::StringUtility::IntegerToString( m_totalCorrectFirstAsk ), "GameStats::AddToTotalCorrectFirstAsk" );
}

void GameStats::AddToWrongVocabulary( std::string topic )
{
    // This currently doesn't take out duplicates,
    // but perhaps it's a useful stat to see how many times
    // you messed up each word.
    m_wrongVocabulary.push_back( topic );
}

void GameStats::AddToTopicTimesAsked( std::string topic )
{
    if ( m_topicStats.find( topic ) == m_topicStats.end() )
    {
        // First time asking
        m_topicStats[topic].totalTimesAsked = 1;
    }
    else
    {
        m_topicStats[topic].totalTimesAsked++;
    }

    chalo::Logger::Out( "Topic " + topic + " times asked is now " + chalo::StringUtility::IntegerToString( m_topicStats[topic].totalTimesAsked ), "GameStats::AddToTopicTimesAsked" );
}

void GameStats::AddToTopicTimesRight( std::string topic )
{
    m_topicStats[topic].totalTimesRight++;
    chalo::Logger::Out( "Topic " + topic + " times right is now " + chalo::StringUtility::IntegerToString( m_topicStats[topic].totalTimesRight ), "GameStats::AddToTopicTimesRight" );
}

void GameStats::AddToTopicTimesWrong( std::string topic )
{
    m_topicStats[topic].totalTimesWrong++;
    chalo::Logger::Out( "Total " + topic + " times wrong is now " + chalo::StringUtility::IntegerToString( m_topicStats[topic].totalTimesWrong ), "GameStats::AddToTopicTimesWrong" );
}

int GameStats::GetTotalQuestionsAsked()
{
    return m_totalQuestionsAsked;
}

int GameStats::GetTotalCorrectFirstAsk()
{
    return m_totalCorrectFirstAsk;
}

const std::vector<std::string>& GameStats::GetWrongVocabulary()
{
    return m_wrongVocabulary;
}

std::string GameStats::GetWrongVocabularyString()
{
    std::string vocab = "";
    for ( unsigned int i = 0; i < m_wrongVocabulary.size(); i++ )
    {
        if ( i != 0 ) { vocab += ", "; }
        vocab += m_wrongVocabulary[i];
    }
    return vocab;
}




